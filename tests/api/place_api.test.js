//-- Dependencies --//
//-- lib dependencies
import app from './../../src/app';
import expect from 'expect.js';
import request from 'supertest';
import { ObjectID } from 'mongodb';
// app dependencies
import { populatesPlaces, places, newPlace } from './../seed/place_seed';
import { Place } from './../../src/models/_index';
import { URL_REQEUST, GOOGLE_API } from './../../src/configs/constants.config';

before(populatesPlaces);

after((done) => {
  Place.remove({}).then(() => done());
});

describe('<-- GET places Google API -->', () => {

  it('should return list of entries and save to db', (done) => {

    request(app)
      .get('/api/places/api?location=-33.8670,151.1957&radius=500&types=food&name=cruise')
      .expect(200)
      .expect((res) => {
        expect(res.body.length).not.to.be(0);
      })
      .end((err, res) => {
        if(err) { return done(err)}

        Place.find({}).then((places) => {
          expect(places.length).not.to.be(0);
          done();
        }, (err) => done(err));
      });
  });

  it('should return list of entries and save to db without duplications', (done) => {
    request(app)
      .get('/api/places/api?location=-33.8670,151.1957&radius=500&types=food&name=cruise')
      .expect(200)
      .expect((res) => {
        expect(res.body.places.length).not.to.be(0);
        expect(res.body.allItems).not.to.be(0);

      })
      .end((err, res) => {
        if(err) { return done(err)}

        Place.find({place_id: res.body.places[0].place_id}).then((places) => {
          expect(places.length).to.be(1);
          done();
        }, (err) => done(err));
      });
  });

  it('should return status 400', (done) => {
    request(app)
      .get('/api/places/api')
      .expect(400)
      .end(done);
  });
});

describe('<-- GET places from db -->', () => {
  it('should return list of entries', (done) => {

    request(app)
      .get('/api/places')
      .expect(200)
      .expect((res) => {
        expect(res.body.length).not.to.be(0);
      })
      .end((err, res) => {
        if(err) { return done(err) }

        done();
      });
  });

  it('should return 404', (done) => {
    Place.remove({}).then();

    request(app)
      .get('/api/places')
      .expect(404)
      .end((err, res) => {
        if(err) { return done(err) }

        done();
      });
  });
});

describe('<-- GET place with id -->', () => {
  beforeEach(populatesPlaces);

  it('should return place', (done) => {

    request(app)
      .get(`/api/places/${ places[1]._id }`)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).to.be(places[1]._id.toHexString());
        expect(res.body.name).to.be(places[1].name);
      })
      .end(done);
  });

  it('should return 404', (done) => {

    request(app)
      .get(`/api/places/${ new ObjectID().toHexString() }`)
      .expect(404)
      .end(done);
  });

  it('should return 400, id is not valid', (done) => {

    request(app)
      .get(`/api/places/1234`)
      .expect(400)
      .end(done);
  });
});

describe('<-- PUT updating place -->', () => {
  let name = 'Some new name';

  it('should update', (done) => {

    request(app)
      .put(`/api/places/${ places[1]._id }`)
      .send({ name })
      .expect(200)
      .expect((res) => {
        expect(res.body.name).to.be(name);
      })
      .end(done);
  });

  it('should return 400 if no data for updating', (done) => {

    request(app)
      .put(`/api/places/${ places[1]._id }`)
      .send()
      .expect(400)
      .end(done);
  });

  it('should return 404', (done) => {

    request(app)
      .put(`/api/places/${ new ObjectID().toHexString() }`)
      .send({ name })
      .expect(404)
      .end(done);
  });

  it('should return 400, id is not valid', (done) => {

    request(app)
      .put(`/api/places/1234`)
      .send({ name })
      .expect(400)
      .end(done);
  });

});

describe('<-- POST adding new place -->', () => {

  it('should add save new place to db', (done) => {

    request(app)
      .post('/api/places')
      .send(newPlace)
      .expect(200)
      .expect((res) => {
        expect(res.body.name).to.be(newPlace.name);
      })
      .end((err, res) => {
        if(err) { return done(err); }

        Place.findOne({ name: newPlace.name })
          .then((placeAdded) => {
            expect(placeAdded.name).to.be(newPlace.name);
            done();
          });
      })
  });

  it('should return 400 if no data for adding', (done) => {

    request(app)
      .post(`/api/places`)
      .send()
      .expect(400)
      .end(done);
  });

  it('should return 400 if try to add place with name which already exists', (done) => {

    request(app)
      .post(`/api/places`)
      .send({ name: places[0].name })
      .expect(400)
      .end(done);
  });

  it('should return db validation error', (done) => {

    request(app)
      .post(`/api/places`)
      .send({ name: 'g' })
      .expect(400)
      .end(done);
  });
});

describe('<-- DELETE removing place from db -->', () => {
  beforeEach(populatesPlaces);

  it('should remove place', (done) => {

    request(app)
      .delete(`/api/places/${ places[0]._id }`)
      .expect(200)
      .expect((res) => {

        Place.findById(places[0]._id)
          .then((place) => {
            expect(place).to.be(null);
          }, (err) => { done(err) });
      })
      .end((err, res) => {
        if(err) { return done(err); }

        done();
      });
  });

  it('should return 404, place not found', (done) => {

    request(app)
      .delete(`/api/places/${ new ObjectID().toHexString() }`)
      .expect(404)
      .end(done);
  });

  it('should return 400, id is not valid', (done) => {

    request(app)
      .delete(`/api/places/1234`)
      .expect(400)
      .end(done);
  });

});
