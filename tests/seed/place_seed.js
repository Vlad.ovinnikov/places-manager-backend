//-- Dependencies --//
// lib dependencies
import { ObjectID } from 'mongodb';
// app dependencies
import { Place } from './../../src/models/_index';

const placeOneId = new ObjectID(),
      placeTwoId = new ObjectID();

const places = [
  {
    _id: placeOneId,
    icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png',
    name: 'Australian Cruise Group',
    place_id: 'ChIJrTLr-GyuEmsRBfy61i59si0',
    reference: 'CmRRAAAAee6q8rkBK-G5e6lsadeA5VXxU4gyv_brmrXUq2SqA7ji8DK6t-1Zp0Rsk38xTZusSzu724utPbJ6yIhV_GGg39ay4TOs_IMllkzrEF1ZPxTQIEQLD1oD94CLLcHr0ndVEhAGhQNs-d2VeS0qhwUvIheYGhQnf7IIEPmwb-mJ8yV9eIgbBHYsSQ',
    vicinity: '32 The Promenade, King Street Wharf 5, Sydney',
    createdAt: '2017-05-26T13:16:56.000Z',
    updatedAt: '2017-05-26T13:16:56.000Z',
    types: [ 'travel_agency', 'restaurant', 'food', 'point_of_interest', 'establishment' ],
    opening_hours: {
      open_now: false,
      weekday_text: []
    },
    photos: [
      {
        height: 2448,
        photo_reference: 'CmRYAAAAuoVkgwANBBXbm_OLmRk8QF5qr5C3FuDAcP1j4KvG9X_wt9DSwmtbhu4mC_ywl9l3lFjhPWQPd6ncIiFc2_I4jlAPtSLqjzAhlDcAnXq5HseiCfl0pIPe_UJtoUwbLWlpEhCR3ouxeHctHR5I7QqRlBYDGhSZAgzdtqBcXjkpzYQBKs0MZYsL8g',
        width: 3264,
        _id: '59282ad524604f9285c6f6a0',
        html_attributions: [
          '<a href=\"https://maps.google.com/maps/contrib/118410684014024830328/photos\">Michael Mak</a>'
        ]
      }
    ],
    geometry: {
      location: {
        lat: -33.867591,
        lng: 151.201196
      }
    }
  },
  {
    _id: placeTwoId,
    icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png',
    name: 'Sydney New Year\'s Eve Cruises',
    place_id: 'ChIJ__8_hziuEmsR27ucFXECfOg',
    reference: 'CmRSAAAAFaFQbDxFbMBO8rV8IzOs8Mfbe48SWmTOj6MBBS5r0LK5YRCtP9z85z1JEVTpzfhoI6ZXPNHmTEv_BkmDIMLumXZs_haxmkLx5xwOw3ipv1TgwbTsQPcx7kTJ0lvifWK8EhD3AvlEsp6mXsNWegzekFrbGhR_H8qLrV4zjLU5lJRTuqB9tau8fA',
    vicinity: '32 The Promenade, Sydney',
    createdAt: '2017-05-26T13:16:56.000Z',
    updatedAt: '2017-05-26T13:16:56.000Z',
    types: [ 'travel_agency', 'restaurant', 'food', 'point_of_interest', 'establishment' ],
    opening_hours: {
      open_now: false,
      weekday_text: []
    },
    photos: [
      {
        height: 1152,
        photo_reference: 'CmRYAAAA12nsZwlb6YLu46JVnmfe4Ndz9CgvB3rS_gDgnYKQhfXteMZ8DC055UDCpnjGeY6ozFlw4Vyi5NU091J0CR74LFLya7cdzpxN7JWcclfcTb9c1IkCv-YqRok_dFWsM1-0EhCG9n54vyKh2xyVw_3wvyL3GhTs-24yUj-yqO_Wp94av_y8JSvLmQ',
        width: 2048,
        _id: '59282ad524604f9285c6f6a6',
        html_attributions: [
          '<a href=\"https://maps.google.com/maps/contrib/107666140764925298472/photos\">Sydney New Year&#39;s Eve Cruises</a>'
        ]
      }
    ],
    geometry: {
      location: {
        lat: -33.8677371,
        lng: 151.2016935
      }
    }
  }
];

let newPlace = {
  icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png',
  name: 'Australian Cruise Group #2',
  place_id: 'ChIJrTLr-GyuEmsRBfy61i59si02',
  reference: 'CmRRAAAAee6q8rkBK-G5e6lsadeA5VXxU4gyv_brmrXUq2SqA7ji8DK6t-1Zp0Rsk38xTZusSzu724utPbJ6yIhV_GGg39ay4TOs_IMllkzrEF1ZPxTQIEQLD1oD94CLLcHr0ndVEhAGhQNs-d2VeS0qhwUvIheYGhQnf7IIEPmwb-mJ8yV9eIgbBHYsSQ2',
  vicinity: '35 The Promenade, King Street Wharf 5, Sydney',
  types: [ 'travel_agency', 'restaurant', 'food', 'point_of_interest', 'establishment' ],
  opening_hours: {
    open_now: false,
    weekday_text: ['monday', 'friday', 'wednesday']
  }
};

const populatesPlaces = (done) => {
  Place.remove({})
    .then(() => {
      let placeOne = new Place(places[0]).save(),
          placeTwo = new Place(places[1]).save();

      return  Promise.all([placeOne, placeTwo]);
    })
    .then(() => done());
}

export { places, populatesPlaces, newPlace };
