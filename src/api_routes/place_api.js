//-- Dependencies --//
// lib dependencies
import _ from 'underscore';
import moment from 'moment';
import request from 'request';
import { ObjectID } from 'mongodb';

// app dependencies
import { Place } from './../models/_index';
import { URL_PLACES_REQEUST, URL_IMAGE_REQEUST, GOOGLE_API } from './../configs/constants.config';

/**
 * get places with Google API
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     list of places
 */
function getWithAPI(req, res) {
  // get all query params
  let queryParams = _.pick(req.query, ['location', 'radius', 'types', 'name', 'page', 'perPage']);

  // build url for request
  let url = `${ URL_PLACES_REQEUST }?location=${ queryParams.location }&radius=${ queryParams.radius }&types=${ queryParams.types }&name=${ queryParams.name }&key=${ GOOGLE_API }`;
  let json = true;

  // make request for Google API
  request({
    url, json
  }, (error, response, body) => {
      // if error just return
      if(body.status === 'INVALID_REQUEST') {
        return res.status(400).send('Something wrong in your request');
      }

      if(body.results.length < 1) {
        return res.status(404).send('There is no result. Try another key word');
      }
      // array of all promises
      let promiseArr = [];
      if(!queryParams.page) {
        queryParams.page = 1;
      }
      if(!queryParams.perPage) {
        queryParams.perPage = 8;
      }
      let startItem = (queryParams.page - 1) * queryParams.perPage;
      let endItem = startItem + queryParams.perPage;
      let slicedArr = body.results.slice(startItem, endItem);
      // go for each palces
      _.each(slicedArr, (place) => {
        // wrap each response place to Mongoose Place object
        let newPlace = new Place(place);
        // store promise to variable
        let placePromise = new Promise((resolve, reject) => {
          // find place in db
          Place.findOne({ place_id: newPlace.place_id })
            .then((existedPlace) => {
              // if place does not exist, just save to db
              if(!existedPlace) {
                newPlace.save().then((data) => {
                  resolve(data);
                })
                .catch((err) => reject(err));

              } else {
                // just return place to user without saving
                resolve(existedPlace);
              }
            }, (err) => reject(err));
        });
        // add each stored promise to the array of promises
        promiseArr.push(placePromise);
      });

      // wait for resolving all promises
      Promise.all(promiseArr).then((values) => {
        // return result to user
        return res.status(200).send({ places: values, allItems: body.results.length });
      })
      .catch((err) => res.send(err));

  });
}

/**
 * get all places from db
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     list of places
 */
function getAll (req, res) {

  Place.find()
    .sort({ 'name': 'asc' })
    .then((places) => {

      // if no places in db return message
      if(places.length === 0) {
        return res.status(404).send('There is no one place exists!');
      }

      res.status(200).send(places);

    }, (err) => res.send(err));
}

/**
 * get place with its id
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     place item
 */
function get(req, res) {

  let id = req.params.id;

  // check if id is valid mongodb id
  if (!ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }

  Place.findOne({ _id: id })
    .then((place) => {

      // if no place found return message
      if(!place) {
        return res.status(404).send({message: 'The requested place does not exist'});
      }

      res.status(200).send(place);

    }, (err) => res.send(err));
}

/**
 * update place with its id
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     updated place
 */
function update(req, res) {
  // store params to the variables
  let id = req.params.id,
      body = _.pick(req.body, ['icon', 'name', 'vicinity', 'types', 'opening_hours', 'photos', 'geometry']);
  // check if id is valid mongodb id
  if (!ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }
  // if no data comes from body return error message
  if(_.isEmpty(body)) {
    return res.status(400).send('No data for updating');
  }

  Place.findByIdAndUpdate(id, {$set: body}, { new: true })
    .then((place) => {
      // if no place found error message
      if(!place) {
        return res.status(404).send('The requested place does not exist');
      }
      // success
      res.status(200).send(place);
    }, (err) => res.send(err));
}

/**
 * add new place
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 */
function add(req, res) {
  // store body to the variable
  let body = _.pick(req.body, ['icon', 'name', 'vicinity', 'types', 'opening_hours', 'photos', 'geometry']);
  let place = new Place();
  place.icon = body.icon;
  place.name = body.name;
  place.vicinity = body.vicinity;
  place.types = body.types;
  place.opening_hours = body.opening_hours;
  place.photos = body.photos;
  place.geometry = body.geometry;

  // if no data comes from body return error message
  if(_.isEmpty(body)) {
    return res.status(400).send('No data for adding');
  }

  Place.findOne({ name: place.name })
    .then((existingPlace) => {

      // if place already exists error message
      if(existingPlace) {
        return res.status(400).send('Place with that name already exists');
      }

      // save new place to db
      place.save().then((newPlace) => {

        res.status(200).send(newPlace);
      })
      .catch((err) => res.status(400).send('Something wrong was happen. Can not save place'));

    }, (err) => res.status(400).send(err));
}

function remove(req, res) {
  let id = req.params.id;
  // check if id is valid mongodb id
  if (!ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }

  // find place in db
  Place.findById(id)
    .then((place) => {
      // if no place found send error message
      if(!place) { return res.status(404).send('Place not found'); }

      // if place exists in db just remove
      place.remove().then(() => {

        res.status(200).send('Place successfully removed');
      })
      .catch((err) => res.status(400).send(err));

    }, (err) => res.status(400).send(err));
}

//-- Exports --//
export default { getWithAPI, getAll, get, update, add, remove };
