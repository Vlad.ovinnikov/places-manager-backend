//-- Dependencies --//
// lib dependencies
import express from 'express';
// app dependencies
import { placeAPI } from './_index';

/**
 * routes
 * @param  {[type]} app app dependency
 */
function routes(app) {
  // API routes
  let router = express.Router();
  app.use('/api', router);

  //-- Place
  router.get('/places/api', placeAPI.getWithAPI);
  router.get('/places', placeAPI.getAll);
  router.get('/places/:id', placeAPI.get);
  router.put('/places/:id', placeAPI.update);
  router.post('/places', placeAPI.add);
  router.delete('/places/:id', placeAPI.remove);

}

//-- Exports --//
export { routes };
