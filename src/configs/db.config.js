//-- Dependencies --//
import { configs } from './config';

//-- Implementation --//
// Set environment for testing db
let env = process.env.NODE_ENV || 'development';

if(env === 'development' || env === 'test') {
  let envConfig = configs[env];

  Object.keys(envConfig).forEach((key) => {
    process.env[key] = envConfig[key];
  });
}
