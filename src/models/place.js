//-- Dependencies --//
// lib dependencies
import mongoose from 'mongoose';
import moment from 'moment';

//-- Implementation --//
const { Schema } = mongoose;

let PlaceSchema = new Schema(
  {
    name: { type: String, minlength: 3, required: true },
    icon: { type: String },
    geometry: {
      location: {
        lat: { type: Number },
        lng: { type: Number }
      }
    },
    photos: [
      {
        height: { type: Number },
        html_attributions: [{ type: String }],
        photo_reference: { type: String },
        width: { type: Number },
        url: { type: String }
      }
    ],
    opening_hours: {
      open_now: { type: Boolean },
      "weekday_text":[
        { type: String }
      ]
    },
    place_id: { type: String },
    reference: { type: String },
    types: [{ type: String }],
    vicinity: { type: String },
    updatedAt: { type: Date, default: moment().format() },
    createdAt: { type: Date, default: moment().format() }
  }
);

PlaceSchema.pre('save', updateDate);

/**
 * update date
 * @param  {Function} next
 * @return {[type]}
 */
function updateDate(next) {
  let place = this;
  place.updatedAt = moment().format();
  next();
}

/**
 * Exports Place model
 * @type {[type]}
 */
let Place = mongoose.model('Place', PlaceSchema);

//-- Exports --//
export default Place;
