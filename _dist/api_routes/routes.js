'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routes = undefined;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _index = require('./_index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * routes
 * @param  {[type]} app app dependency
 */
//-- Dependencies --//
// lib dependencies
function routes(app) {
  // API routes
  var router = _express2.default.Router();
  app.use('/api', router);

  //-- Place
  router.get('/places/api', _index.placeAPI.getWithAPI);
  router.get('/places', _index.placeAPI.getAll);
  router.get('/places/:id', _index.placeAPI.get);
  router.put('/places/:id', _index.placeAPI.update);
  router.post('/places', _index.placeAPI.add);
  router.delete('/places/:id', _index.placeAPI.remove);
}

//-- Exports --//

// app dependencies
exports.routes = routes;