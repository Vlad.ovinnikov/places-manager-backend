'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _mongodb = require('mongodb');

var _index = require('./../models/_index');

var _constants = require('./../configs/constants.config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * get places with Google API
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     list of places
 */


// app dependencies
//-- Dependencies --//
// lib dependencies
function getWithAPI(req, res) {
  // get all query params
  var queryParams = _underscore2.default.pick(req.query, ['location', 'radius', 'types', 'name', 'page', 'perPage']);

  // build url for request
  var url = _constants.URL_PLACES_REQEUST + '?location=' + queryParams.location + '&radius=' + queryParams.radius + '&types=' + queryParams.types + '&name=' + queryParams.name + '&key=' + _constants.GOOGLE_API;
  var json = true;

  // make request for Google API
  (0, _request2.default)({
    url: url, json: json
  }, function (error, response, body) {
    // if error just return
    if (body.status === 'INVALID_REQUEST') {
      return res.status(400).send('Something wrong in your request');
    }

    if (body.results.length < 1) {
      return res.status(404).send('There is no result. Try another key word');
    }
    // array of all promises
    var promiseArr = [];
    if (!queryParams.page) {
      queryParams.page = 1;
    }
    if (!queryParams.perPage) {
      queryParams.perPage = 8;
    }
    var startItem = (queryParams.page - 1) * queryParams.perPage;
    var endItem = startItem + queryParams.perPage;
    var slicedArr = body.results.slice(startItem, endItem);
    // go for each palces
    _underscore2.default.each(slicedArr, function (place) {
      // wrap each response place to Mongoose Place object
      var newPlace = new _index.Place(place);
      // store promise to variable
      var placePromise = new Promise(function (resolve, reject) {
        // find place in db
        _index.Place.findOne({ place_id: newPlace.place_id }).then(function (existedPlace) {
          // if place does not exist, just save to db
          if (!existedPlace) {
            newPlace.save().then(function (data) {
              resolve(data);
            }).catch(function (err) {
              return reject(err);
            });
          } else {
            // just return place to user without saving
            resolve(existedPlace);
          }
        }, function (err) {
          return reject(err);
        });
      });
      // add each stored promise to the array of promises
      promiseArr.push(placePromise);
    });

    // wait for resolving all promises
    Promise.all(promiseArr).then(function (values) {
      // return result to user
      return res.status(200).send({ places: values, allItems: body.results.length });
    }).catch(function (err) {
      return res.send(err);
    });
  });
}

/**
 * get all places from db
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     list of places
 */
function getAll(req, res) {

  _index.Place.find().sort({ 'name': 'asc' }).then(function (places) {

    // if no places in db return message
    if (places.length === 0) {
      return res.status(404).send('There is no one place exists!');
    }

    res.status(200).send(places);
  }, function (err) {
    return res.send(err);
  });
}

/**
 * get place with its id
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     place item
 */
function get(req, res) {

  var id = req.params.id;

  // check if id is valid mongodb id
  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }

  _index.Place.findOne({ _id: id }).then(function (place) {

    // if no place found return message
    if (!place) {
      return res.status(404).send({ message: 'The requested place does not exist' });
    }

    res.status(200).send(place);
  }, function (err) {
    return res.send(err);
  });
}

/**
 * update place with its id
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 * @return {[type]}     updated place
 */
function update(req, res) {
  // store params to the variables
  var id = req.params.id,
      body = _underscore2.default.pick(req.body, ['icon', 'name', 'vicinity', 'types', 'opening_hours', 'photos', 'geometry']);
  // check if id is valid mongodb id
  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }
  // if no data comes from body return error message
  if (_underscore2.default.isEmpty(body)) {
    return res.status(400).send('No data for updating');
  }

  _index.Place.findByIdAndUpdate(id, { $set: body }, { new: true }).then(function (place) {
    // if no place found error message
    if (!place) {
      return res.status(404).send('The requested place does not exist');
    }
    // success
    res.status(200).send(place);
  }, function (err) {
    return res.send(err);
  });
}

/**
 * add new place
 * @param  {[type]} req for requesting to the params or query params
 * @param  {[type]} res for response result
 */
function add(req, res) {
  // store body to the variable
  var body = _underscore2.default.pick(req.body, ['icon', 'name', 'vicinity', 'types', 'opening_hours', 'photos', 'geometry']);
  var place = new _index.Place();
  place.icon = body.icon;
  place.name = body.name;
  place.vicinity = body.vicinity;
  place.types = body.types;
  place.opening_hours = body.opening_hours;
  place.photos = body.photos;
  place.geometry = body.geometry;

  // if no data comes from body return error message
  if (_underscore2.default.isEmpty(body)) {
    return res.status(400).send('No data for adding');
  }

  _index.Place.findOne({ name: place.name }).then(function (existingPlace) {

    // if place already exists error message
    if (existingPlace) {
      return res.status(400).send('Place with that name already exists');
    }

    // save new place to db
    place.save().then(function (newPlace) {

      res.status(200).send(newPlace);
    }).catch(function (err) {
      return res.status(400).send('Something wrong was happen. Can not save place');
    });
  }, function (err) {
    return res.status(400).send(err);
  });
}

function remove(req, res) {
  var id = req.params.id;
  // check if id is valid mongodb id
  if (!_mongodb.ObjectID.isValid(id)) {
    return res.status(400).send('The requested place id is not correct');
  }

  // find place in db
  _index.Place.findById(id).then(function (place) {
    // if no place found send error message
    if (!place) {
      return res.status(404).send('Place not found');
    }

    // if place exists in db just remove
    place.remove().then(function () {

      res.status(200).send('Place successfully removed');
    }).catch(function (err) {
      return res.status(400).send(err);
    });
  }, function (err) {
    return res.status(400).send(err);
  });
}

//-- Exports --//
exports.default = { getWithAPI: getWithAPI, getAll: getAll, get: get, update: update, add: add, remove: remove };