'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.placeAPI = undefined;

var _place_api = require('./place_api');

var _place_api2 = _interopRequireDefault(_place_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var placeAPI = exports.placeAPI = _place_api2.default;