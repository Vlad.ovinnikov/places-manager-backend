"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var configs = exports.configs = {
  "test": {
    "PORT": "3012",
    "MONGODB_URI": "localhost:27017/places-manager-db-test"
  },
  "development": {
    "PORT": "3012",
    "MONGODB_URI": "localhost:27017/places-manager-db"
  }
};