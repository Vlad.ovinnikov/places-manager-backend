'use strict';

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _app = require('./../app');

var _app2 = _interopRequireDefault(_app);

var _db = require('./../configs/db.config');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// debug('cubic-house-backend:server');

//-- Implememtation --//
// Get port from environment and store in Express.

// app dependencies
//-- Dependencies --//
// lib dependencies
var PORT = normalizePort(process.env.PORT);
_app2.default.set('port', PORT);

// Create HTTP server.
var server = _http2.default.createServer(_app2.default);

// Listen on provided port, on all network interfaces.
server.listen(PORT, function (err) {
  if (err) throw err;

  console.log('Express server listening on port ' + PORT);
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 * @param  {[type]} val [description]
 * @return {[type]}     [description]
 */
function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 * @param  {[type]} error [description]
 * @return {[type]}       [description]
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof PORT === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use ' + _db2.default);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 * @return {[type]} [description]
 */
function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  (0, _debug2.default)('Listening on ' + bind);
}