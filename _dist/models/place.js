'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//-- Implementation --//
//-- Dependencies --//
// lib dependencies
var Schema = _mongoose2.default.Schema;


var PlaceSchema = new Schema({
  name: { type: String, minlength: 3, required: true },
  icon: { type: String },
  geometry: {
    location: {
      lat: { type: Number },
      lng: { type: Number }
    }
  },
  photos: [{
    height: { type: Number },
    html_attributions: [{ type: String }],
    photo_reference: { type: String },
    width: { type: Number },
    url: { type: String }
  }],
  opening_hours: {
    open_now: { type: Boolean },
    "weekday_text": [{ type: String }]
  },
  place_id: { type: String },
  reference: { type: String },
  types: [{ type: String }],
  vicinity: { type: String },
  updatedAt: { type: Date, default: (0, _moment2.default)().format() },
  createdAt: { type: Date, default: (0, _moment2.default)().format() }
});

PlaceSchema.pre('save', updateDate);

/**
 * update date
 * @param  {Function} next
 * @return {[type]}
 */
function updateDate(next) {
  var place = this;
  place.updatedAt = (0, _moment2.default)().format();
  next();
}

/**
 * Exports Place model
 * @type {[type]}
 */
var Place = _mongoose2.default.model('Place', PlaceSchema);

//-- Exports --//
exports.default = Place;