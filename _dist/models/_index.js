'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Place = undefined;

var _place = require('./place');

var _place2 = _interopRequireDefault(_place);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Place = exports.Place = _place2.default;